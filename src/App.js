import React from 'react'
import { BrowserRouter,Routes,Route } from 'react-router-dom'
import Home from './components/Home'
import Protected from './components/Protected'
import Navbar from './components/Navbar'
import Footer from "./components/footer"
import "./App.css"
import Converter from "./components/Converter"
import Login from './components/Login'
import Xmltojson from './components/Xmltojson'
import JsonToXml from './components/JsonToXml'
import PrettyJson from './components/Pretty'
import {ToastContainer} from "react-toastify"
const App = () => {
  return (
    <>
      <BrowserRouter>
      <Navbar />
        <Routes>
          <Route  element={<Protected/>}>
          </Route>
          <Route path='/' element={<Home/>} />
         <Route path="/login" element={<Login/>}/>
          <Route path='/converters' element={<Converter/>} />
          <Route path='/xmltojson' element={<Xmltojson/>} />
          <Route path='/jsontoxml' element={<JsonToXml/>} />
          <Route path='/pretty' element={<PrettyJson/>} />
          <Route path='*' element={<Converter/>} />

        </Routes>
        <Footer />
      </BrowserRouter>
<ToastContainer/>
    </>
  )
}

export default App