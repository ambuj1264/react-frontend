import React from "react";
import { NavLink } from "react-router-dom";
import "./Navbar.css"; // Import the custom CSS file

const Navbar = () => {
  return (
    <>
    
        <nav className="navbar navbar-expand-lg bg-body-tertiary custom-navbar" style={{top:"1px",backgroundColor:"rgb(41, 41, 97) !important"}}>
          <div className="container-fluid" style={{ backgroundColor: "#292961", color: "white" }}>
            <NavLink className="navbar-brand text-light" to="/">
              React Frontend
            </NavLink>
            <button
              className="navbar-toggler"
              type="button"
              data-bs-toggle="collapse"
              data-bs-target="#navbarSupportedContent"
              aria-controls="navbarSupportedContent"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span className="navbar-toggler-icon" style={{backgroundColor:"white"}} />
            </button>
            <div
              className="collapse navbar-collapse custom-navbar-collapse"
              id="navbarSupportedContent"
            >
              <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                <li className="nav-item">
                  <NavLink
                    className="nav-link active"
                    aria-current="page"
                    to="/"
                  >
                    Home
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/login">
                    Login
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/converters">
                    Converters
                  </NavLink>
                </li>
              </ul>
            </div>
          </div>
        </nav>
     
    </>
  );
};

export default Navbar;
