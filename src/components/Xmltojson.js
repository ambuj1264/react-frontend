import axios from "axios";
import React, { useState } from "react";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
const Xmltojson = () => {
  const [xmltojson, setXmltojson] = useState("");
  const [jsontoxml, setJson] = useState("");
  const notify = () => toast.error("XML is not write");
  const handler = (e) => {
    setXmltojson(e.target.value);
  };
  const xmlSubmit = async (e) => {
    e.preventDefault();
    try {
      const responseOfaxios = await axios.post(
        "http://localhost:4000/xmltojson",
        {
          xmltojson,
        }
      );
      console.log(responseOfaxios?.data?.success === true);
      if (responseOfaxios?.data?.success === true) {
        setJson(JSON.stringify(responseOfaxios?.data?.data));
      } else {
        notify();
      }
    } catch (error) {
      notify();
      console.log(error?.message);
    }
  };
  return (
    <>
      <div
        className="d-flex justify-content-center flex-wrap  "
        style={{
          backgroundColor: "black",
          color: "white",
          width: "100%",
          height: "82vh",
        }}
      >
        <div style={{ marginTop: "50px" }} className="">
          <h3 style={{ textAlign: "center" }}>XML</h3>
          <textarea
            name="xmltojson"
            id=""
            cols="60"
            rows="15"
            value={xmltojson}
            onChange={handler}
          />
          <br />
          <button
            className="btn btn-primary d-flex justify-content-center "
            onClick={xmlSubmit}
            style={{ alignItems: "center", marginLeft: "200px" }}
          >
            Convert
          </button>{" "}
          <ToastContainer />
        </div>
        <div style={{ marginTop: "50px" }} className="">
          <h3 style={{ textAlign: "center" }}>JSON</h3>
          <textarea name="" id="" cols="60" rows="15" value={jsontoxml} />
          <br />
        </div>
      </div>
      <div className="container pb-5">
        <h3 style={{ textAlign: "center" }}> Description </h3>
        <p>
          Converting XML to JSON is a valuable process that allows you to
          transform your data into a more versatile and widely supported format.
          XML (eXtensible Markup Language) and JSON (JavaScript Object Notation)
          are both popular data interchange formats, but JSON is often preferred
          for its simplicity and compatibility with various programming
          languages. The conversion from XML to JSON offers several advantages.
          JSON provides a lightweight and easily readable structure, making it
          ideal for data transmission and storage. By converting XML to JSON,
          you can simplify the data representation and eliminate the
          complexities associated with XML's hierarchical nature. Furthermore,
          JSON's compatibility with modern web technologies makes it highly
          beneficial for SEO. Search engines prioritize websites with
          well-structured and accessible content. JSON's straightforward format
          allows search engine crawlers to efficiently parse and understand the
          data, leading to improved indexing and potentially higher search
          rankings. During the XML to JSON conversion, it is crucial to ensure
          data integrity and accuracy. The conversion process should retain all
          essential information from the original XML, avoiding any loss of
          data. This preservation of data integrity helps search engines
          correctly interpret and index the content, contributing to better SEO
          performance. In conclusion, converting XML to JSON brings numerous
          advantages, including improved data representation, compatibility with
          web technologies, and enhanced SEO. By leveraging JSON's simplicity
          and search engine-friendly structure, you can optimize your website's
          visibility and make your content more accessible to both search
          engines and users.
        </p>
      </div>
    </>
  );
};

export default Xmltojson;
