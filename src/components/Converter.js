import React from "react";
import SubConverter from "./SubConverter";
import image1 from "../asset/JSONConverter_online.png";
import image2 from "../asset/json_xml.png";
const Converter = () => {
  return (
    <>
      <h3 style={{ textAlign: "center" }}>
        <u>Converter With Favourite Tool</u>
      </h3>
      <div className="d-flex justify-content-evenly flex-wrap">
       <SubConverter image = {{image1}} data= {{heading:"XML To JSON" , button:"/xmltojson" ,quote:" Please provide the XML data containing the quote, and I'll assist you in converting it to JSON format"}}/>
       <SubConverter image = {{image2}} data= {{heading:"JSON To XML", button:"/jsontoxml" ,quote:" Please provide the JSON data containing the quote, and I'll assist you in converting it to XML format" }}/>
       {/* <SubConverter image = {{image1}} data= {{heading:"JSON To XML" }}/> */}
      </div>
    </>
  );
};

export default Converter;
