import React, { useState } from 'react'
import { useNavigate, Link } from 'react-router-dom'
import "./style/home.css"
import {  toast } from 'react-toastify';
// import 'ReactToastify.css';
function Login() {
  const navigate = useNavigate();
  const [state, setState] = useState({
    email: "", password: ""
  })

  const [emailMessage, setEmailMessage] = useState("")
  const [passwordMessage, setPasswordMessage] = useState("")
  const [eyetype, seteyetype] = useState(true);
  const [eye, setEye] = useState(true)
  const handler = (e) => {
    setState({ ...state, [e.target.name]: e.target.value })
  }

  const submitData = async () => {
    const { email, password } = state
    const emailRegExp = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;
    const passwordRegExp = /^[a-zA-Z]\w{3,14}$/;
    //////////email validation//////////////////
    if (email === "") {
      setEmailMessage("Email field is not blank")
      return false
    }
    else if (!emailRegExp.test(email)) {
      setEmailMessage("Email is not valid")
      return false
    }
    else {
      setEmailMessage("")
    }
    ///////////password Validation//////////////////////
    if (password === "") {
      setPasswordMessage("Password field is not blank")
      return false
    }
    else if (!passwordRegExp.test(password)) {
      setPasswordMessage("Your password sholud be first latter or 4 to 15 charecter only")
      return false
    }
    else {
      setPasswordMessage("")
    }
    /////////////login functionality////////////
    if (email && password) {
      try {
        let result = await fetch('http://localhost:4500/login', {
          method: 'POST',
          body: JSON.stringify({ email, password }),
          headers: {
            'Content-type': 'application/json; charset=UTF-8',
          },
        });
        let data = await result.json();
        console.log(data.result, "data")
        if (data.result) {
          localStorage.setItem("email", data.result.email)
          localStorage.setItem("token", data.token)
          navigate("/product")
        }
        else {
          toast.error("email or password is not matched", {
            position: toast.POSITION.TOP_RIGHT,
            className: 'toast-message'
        });
        //   alert("email or password is not matched")
        }

      } catch (error) {
        console.log(error)
        // alert("email or password is not matched")
        toast.error("email or password is not matched", {
          position: toast.POSITION.TOP_RIGHT,
          className: 'toast-message'
      });
      }

    }
    else {
      // alert("All field is required")
      toast.error('"All field is required"!', {
        position: toast.POSITION.TOP_RIGHT,
        className: 'toast-message'
    });
    }
    // setState({email:"",password:""})
  }

  const eyeClick = (e) => {
    e.preventDefault();
    seteyetype(!eyetype)
    setEye(!eye)
  }

  return (
    <>
      <div className='container'>
        <div className='row'>
          <h1 className='text-center'>Login Here</h1>
          <div className='col-sm-8 col-md-8 mx-auto'>
            Email:<input
              type="email"
              name="email"
              className='form-control my-3'
              placeholder="Your Email Id"
              value={state.email}
              onChange={handler}

            />
            <div style={{ color: "red" }}>{emailMessage}</div>
            Password:  <div className='d-flex justify-content-sm-between'>
            <input
              type={eyetype ? "password" : "text"}
              name="password"
              className='form-control my-3'
              placeholder="Your Password"
              value={state.password}
              onChange={handler}
            />
            <span onClick={eyeClick} className='eye'>
              {
                eye ? <i className="fa-solid fa-eye-slash fa-bounce" style={{ color: '#36ce4f' }}>
                </i>
                  :
                  <i className="fa-solid fa-eye fa-bounce" style={{ color: '#40e743' }} />
              }

            </span>
            </div>
            <div style={{ color: "red" }}>{passwordMessage}</div>
            <button
              className='btn btn-primary'
              type='button'
              onClick={submitData}
            >Login</button>
            <span style={{ paddingLeft: "40px" }}>If you have not account please</span> <Link to="/register">Register</Link>
            <span style={{ paddingLeft: "40px" }}></span> <Link to="/reset">Forget your password</Link>
          </div>
        </div>
      </div>
    </>
  )
}

export default Login