import React from 'react';

const json = {
  "name": "John Doe",
  "age": 32,
  "email": "johndoe@example.com"
};

const PrettyJson = () => {
  return (
    <pre>{JSON.stringify(json, null, 2)}</pre>
  );
};

export default PrettyJson;