// postSaga.js

import { put, takeLatest } from 'redux-saga/effects';
import { POST_REQUEST, postSuccess, postFailure } from './postActions';

function* postSagaWorker(action) {
  try {
    // Simulate an API call with a delay
    yield new Promise(resolve => setTimeout(resolve, 2000));
    // Handle successful response
    yield put(postSuccess());
  } catch (error) {
    // Handle error
    yield put(postFailure(error.message));
  }
}

function* postSagaWatcher() {
  yield takeLatest(POST_REQUEST, postSagaWorker);
}

export default postSagaWatcher;
