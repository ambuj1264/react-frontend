// postActions.js

export const POST_REQUEST = 'POST_REQUEST';
export const POST_SUCCESS = 'POST_SUCCESS';
export const POST_FAILURE = 'POST_FAILURE';

export const postRequest = payload => ({
  type: POST_REQUEST,
  payload,
});

export const postSuccess = () => ({
  type: POST_SUCCESS,
});

export const postFailure = error => ({
  type: POST_FAILURE,
  error,
});
