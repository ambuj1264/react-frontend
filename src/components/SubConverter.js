import React from "react";

import { Link } from "react-router-dom";
const SubConverter = (props) => {
  return (
    <>
      <div className="card" style={{ width: "18rem" , }}>
        <img src={props?.image?.image1 ? props?.image?.image1 :props?.image?.image2} className="card-img-top" alt="..." style={{height: "7.688rem"}} />
        <div className="card-body">
          <h5 className="card-title">{props?.data?.heading}</h5>
          <p className="card-text">
           {props?.data?.quote}
          </p>
          <div >
          <Link to={props?.data?.button} className="btn btn-primary" style={{marginLeft:"70px"}} >
            Convert
          </Link></div>
        </div>
      </div>
     
    </>
  );
};

export default SubConverter;
