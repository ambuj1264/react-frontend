import React from 'react'
import { Outlet , Navigate} from 'react-router-dom'
const Protected = () => {
  const authKey = localStorage.getItem("tokens");
  console.log(authKey,"authKey");
  return authKey? <Outlet /> : <Navigate to="/login"></Navigate>
}
export default Protected