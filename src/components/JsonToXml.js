import axios from "axios";
import React, { useState } from "react";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
const JsonToXml = () => {
  const [jsontoxml, setJsontoxml] = useState("");
  const [json, setJson] = useState("");
  const notify = () => toast.error("JSON is not write");
  const handler = (e) => {
    setJsontoxml(e.target.value);
  };
  const xmlSubmit = async (e) => {
    e.preventDefault();
    try {
      console.log(jsontoxml)
      const responseOfaxios = await axios.post(
        "http://localhost:4000/jsontoxml",
        {
          jsontoxml ,
        }
      );
      console.log(responseOfaxios?.data?.success === true);
      if (responseOfaxios?.data?.success === true) {
        setJson((responseOfaxios?.data?.data));
      } else {
        notify();
      }
    } catch (error) {
      notify();
      console.log(error?.message);
    }
  };
  return (
    <>
      <div
        className="d-flex justify-content-center flex-wrap  "
        style={{
          backgroundColor: "black",
          color: "white",
          width: "100%",
          height: "82vh",
        }}
      >
        <div style={{ marginTop: "50px" }} className="">
          <h3 style={{ textAlign: "center" }}>JSON</h3>
          <textarea
            name="xmltojson"
            id=""
            cols="60"
            rows="15"
            value={jsontoxml}
            onChange={handler}
          />
          <br />
          <button
            className="btn btn-primary d-flex justify-content-center "
            onClick={xmlSubmit}
            style={{ alignItems: "center", marginLeft: "200px" }}
          >
            Convert
          </button>{" "}
          <ToastContainer />
        </div>
        <div style={{ marginTop: "50px" }} className="">
          <h3 style={{ textAlign: "center" }}>XML</h3>
          <textarea name="" id="" cols="60" rows="15" value={json} />
          <br />
        </div>
      </div>
      <div className="container pb-5">
        <h3 style={{ textAlign: "center" }}> Description </h3>
        <p>
          Converting JSON to XML is also a valuable process that allows you to
          transform your data into a different format. While JSON is known for
          its simplicity and compatibility with various programming languages,
          XML (eXtensible Markup Language) has its own set of advantages and use
          cases. XML is a widely used data interchange format, particularly in
          industries such as publishing, finance, and healthcare. It offers a
          hierarchical structure that allows for more complex data
          representations and supports features like namespaces and schemas.
          Converting JSON to XML can be beneficial in scenarios where XML is the
          preferred format for data exchange or when existing systems or APIs
          require XML input. Here are a few advantages of converting JSON to
          XML: Compatibility: XML is widely supported by various programming
          languages, libraries, and tools. If you're working with systems that
          rely on XML parsing and processing, converting JSON to XML allows for
          seamless integration and interoperability. Complex data structures:
          XML's hierarchical nature makes it suitable for representing complex
          data structures with nested elements and attributes. If your JSON data
          contains intricate relationships or metadata that can be better
          represented using XML's tree-like structure, converting to XML can
          preserve the full fidelity of the original data. Standards compliance:
          In some domains, XML is a standard format for data interchange and
          communication. By converting JSON to XML, you ensure compliance with
          industry-specific standards and enable seamless data integration with
          existing systems. Transformation and processing: XML provides a rich
          set of tools and technologies for data transformation and processing,
          such as XSLT (eXtensible Stylesheet Language Transformations) and
          XPath (XML Path Language). Converting JSON to XML opens up the
          possibility of using these powerful tools for data manipulation,
          extraction, and reporting. When converting JSON to XML, it is
          important to ensure the integrity and accuracy of the data. The
          conversion process should preserve all essential information from the
          original JSON, avoiding any loss of data. This is especially crucial
          when dealing with complex data structures and nested elements. In
          conclusion, while JSON is often preferred for its simplicity and
          compatibility, converting JSON to XML can be advantageous in scenarios
          where XML's hierarchical structure, compatibility with existing
          systems, standards compliance, or data transformation capabilities are
          required. Understanding the specific requirements of your use case
          will help determine whether converting JSON to XML is the appropriate
          choice for your data transformation needs.
        </p>
      </div>
    </>
  );
};

export default JsonToXml;
